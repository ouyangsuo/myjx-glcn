const fs = require("fs");
const path = require("path");

/* 去除图片名称的多级后缀 */
function renameImgExtnames(dirpath) {
    // const reg = /![^\.]*\.webp/;
    const reg = /\.(jpg|png|gif|webp|svg)(.+)/;
    let items = fs.readdirSync(dirpath);
    items = items
        .sort(
            (a, b) =>
                fs.statSync(path.resolve(dirpath, a)).mtime -
                fs.statSync(path.resolve(dirpath, b)).mtime
        )
        .filter((filename) => reg.test(filename));
    // console.log(items);

    items.forEach((filename) => {
        const newname = filename.replace(reg, function (sub) {
            const tail = reg.exec(filename)[2];
            return sub.replace(tail, "");
        });
        fs.renameSync(
            path.join(dirpath,filename),
            path.join(dirpath,newname),
        )
    });
    console.log("rename done!");
}
// renameImgExtnames("D:\\GZH5\\H5-2111\\code\\W8_项目周\\mypro\\client\\assets");

function editFileUpdateImgExtnames(filepath){
    const reg = /\.(jpg|png|gif|webp|svg)([^"'\)};]+)["']/g;
    const reg2 = /\.(jpg|png|gif|webp|svg)([^"'\)};]+)["']/;
    let content = fs.readFileSync(filepath).toString()

    content = content.replace(reg,function(sub){
        const temp = reg2.exec(sub)
        console.log(sub,temp);

        const rabbish = temp[2]
        return sub.replace(rabbish,"")
    })

    fs.writeFileSync(filepath,content)
    console.log("job done");
}
// editFileUpdateImgExtnames("D:\\GZH5\\H5-2111\\code\\W8_项目周\\mypro\\client\\src\\views\\pages\\index.html")