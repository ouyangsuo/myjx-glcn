# 工作目录的修改添加到暂存区【代码还未受控】
git add .
# 将暂存区内容推本地仓库【已受控】
git commit -m "提交信息"
git commit -am 'removed test.txt、add runoob.php'

# 配置SSH公钥
# 生成公钥
ssh-keygen -t rsa -C "youremail@example.com"
ssh-keygen -t rsa -C "284577461@qq.com"
cd .ssh
type id_rsa.pub 并复制内容


git init
git clone https://gitee.com/steveouyang/test-repo.git

# ping不通github时修改系统hosts文件
# （IP/hosts查询：https://ipaddress.com/website/gitlab.com https://ipaddress.com/website/gitlab.global.ssl.fastly.net）
140.82.114.4 github.com
199.232.69.194 github.global.ssl.Fastly.net

# 做全局用户配置
git config --global user.name "yourname"
git config --global user.email yourname@sina.com
git config --list

# 配置当前工程的用户信息
git config user.name "yourname"
git config user.email yourname@sina.com
git config --local --list

# 查看仓库当前的状态，显示有变更的文件
git status	

# 比较文件的不同，即暂存区和工作区的差异。
# 当命令行底部显示【冒号:】说明目前处于大块信息的浏览模式 输入指令q 即可退出到正常的命令模式
git diff	

# 查看版本日志
git log

# 硬回退到最后一次提交的版本
git reset --hard HEAD
# 硬回退到指定版本
git reset --hard 5c040c695267cbcca01517ca1c1618d17f43dd32

# 从工作区和暂存区删除文件
git rm -f b.js

# 显示远程源的信息
git remote show

# 添加远程源（首先要手动创建远程仓库）
git remote add origin https://gitlab.com/ouyangsuo/myjx-gl.git
git remote add origin git@gitlab.com:ouyangsuo/myjx.git
git remote -v
git remote remove origin
git remote add origin xxx
git config --system --unset credential.helper

# 将本地代码master分支推送到远程仓库origin（首次关联时可能403权限错误）
git push -u origin master

############推送推送远程时的403错误############
1.修改本地仓库配置文件 .git/config
[remote "origin"]
url = https://gitee.com/steveouyang/testpro2106.git 
修改为
url = https://你的用户名@gitee.com/steveouyang/testpro2106.git

2.拉取远程仓库origin的master分支（允许不相干的历史）这步之后就成功关联了远程仓库
git pull origin master --allow-unrelated-histories
git pull origin main --allow-unrelated-histories

3.再次推送远程
route add 172.65.251.78 10.3.140.1 -p
git push -u origin master
或
git branch -M main
git push -u origin main
# 
git push --set-upstream origin master
#################################################

################VSCode中文件的状态################
U = Unstaged未添加到暂存区
A = added 已添加到暂存区
M = modified 有未提交的修改
#################################################

git branch
git branch dev
git branch feature-login
git branch feature-goods
git checkout -b feature-weather
git branch -d feature-weather
git branch -D feature-weather

# 在feature-login分支上完成开发并推送远程
git checkout feature-login
git commit -am "完成登录功能开发"
git push origin feature-login

# 在dev分支上合并feature-login
git checkout dev
git merge feature-login

# 在feature-goods分支上完成开发并推送远程
git checkout feature-goods
git add . 
#如果报错：warning: LF will be replaced by CRLF in src/views/pages/index.html. 
git config --global core.autocrlf true
git commit -m "完成商品增删改查接口开发"
git push origin feature-goods

# 在dev分支上合并feature-login（可能会产生冲突）
git checkout dev
git merge feature-goods
######################解决冲突###################
协商修改冲突位置的代码
git add .
git commit -m "解决冲突完成合并并提交"
#################################################
git push origin dev

# master分支合并dev分支 打tag 上线
git checkout master
git merge dev
git commit -am "完成1.0版本开发"
git push origin master
git log
git tag -a v1.0 4c00384b7 -m "这是1.0标签信息"
git push origin master --tags


# 团队项目合并分支 
git checkout dev / 建议将自己的特性分支复制一份进行练习： git checkout -b mydev
# !!!!!把dev分支没有提的内容连提带推(充分保证劳动成果已经落实到版本历史)!!!!!非常重要!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
git add .
git commit -m "预备合并"
git push -u origin dev
# 将所有远程内容拉取到工作目录
git fetch
# 用dev分支去吞并所有特性分支（其间会有冲突，解决完之后：删除冲突标记=>提交=>重新合并）
git merge feature1
git merge feature2
git merge feature3
# dev通过测试后
git checkout master
git merge dev