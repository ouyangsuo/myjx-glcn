import '../css/first-screen.chunk.css'
import '../css/index.chunk.css'
import '../css/seckill.chunk.css'
import '../css/core1.chunk.css'
import '../css/nice_goods.chunk.css'
import '../css/core2.chunk.css'
import '../css/swiper-bundle.min.css'
import '../css/index.css'

import { ajaxPromise } from "../../utils/ajax_util.js";
import MySwiper from "../../components/myswiper.js";

// swiper
import Swiper from "../../vendors/swiper-bundle.esm.browser.min.js";
import { baseUrl } from '../../conf/http_conf'

function renderChannels(res) {
    const { data } = res;
    data.forEach((channel, index) => {
        const elem = document.querySelector(`.channels_item_${index + 1}`);
        if (index < 2) {
            elem.style.setProperty(
                "background",
                `url('https:${channel.bigImg}') 0 0/100% no-repeat`,
                "important"
            );
        } else {
            elem.id = "mine";
            // elem.style = {
            //     display:"flex",
            //     flexDirection: "column",
            // }

            elem.innerHTML = `
        <a class="channels_item_title" href="//b.jd.com/?skuId=5123256_5436612&amp;groupId=03312682&amp;productId=09180689"
            target="_blank" clstag="h|keycount|chan|bi#01_a">
            <span class="channels_item_title_main">${channel.name}</span>
            <span class="channels_item_title_aside">${channel.desc}</span>
        </a>
        
        <div class="channels_item_imgs">
            <a class="channels_item_link" href="//b.jd.com/?skuId=5123256_5436612&amp;groupId=03312682&amp;productId=09180689"
                target="_blank" clstag="h|keycount|chan|bi#01_b01" tabindex="-1">
                <div class="lazyimg lazyimg_loaded channels_item_img">
                    <img src="//img13.360buyimg.com/img/s200x200_${channel.item[0].img}!cc_100x100.webp"
                        class="lazyimg_img">
                </div>
            </a>
        
            <a class="channels_item_link" href="//b.jd.com/?skuId=5436612_5123256&amp;groupId=03312682&amp;productId=09180689"
                target="_blank" clstag="h|keycount|chan|bi#01_b02" tabindex="-1">
                <div class="lazyimg lazyimg_loaded channels_item_img">
                    <img src="//img30.360buyimg.com/img/s200x200_${channel.item[1].img}!cc_100x100.webp"
                        class="lazyimg_img">
                </div>
            </a>
        </div>            
        `;
        }
    });
}

function renderMore2goods(res) {
    const { data } = res;

    let listHtml = "";
    data.forEach((item) => {
        listHtml += `
            <div class="more2_item more2_item_good skeleton-block">
            <div class="more2_img skeleton-element">
                <img src="//img14.360buyimg.com/jdcms/s150x150_${item.img}"
                    class="lazyimg_img123" alt="${item.t}">
            </div>
            <div class="more2_info">
                <p class="more2_info_name">${item.t}</p>
                <div class="more2_info_price more2_info_price_plus more2_info_price_newcomer">
                    <div class="mod_price">
                        <i>¥</i>
                        <span class="more2_info_price_txt">${item.op.slice(
                            0,
                            -2
                        )}
                            <span class="more2_info_price_txt-decimal">${item.op.slice(
                                -2
                            )}</span>
                        </span>
                    </div>
                </div>
            </div>
        </div>            
            `;
    });
    document.querySelector(".more2_list").innerHTML = listHtml;
}

let sliderWrapper;
let bannerSwiper;
function renderBanner(res) {
    const { data } = res;
    sliderWrapper = document.querySelector("#slider_wrapper");

    let lisHtml = "";
    data.forEach((item) => {
        lisHtml += `
        <li class="slider_item focus-slider__item focus-item slider_active" data-index="0"
            style="float: left; position: relative; transition: opacity 300ms ease-in-out 0s; width: 590px; left: 0px; opacity: 1; z-index: 1;">
            <a href="https://pro.jd.com/mall/active/3ZYfZKGRAhbHzJySpRriJoGWo8v6/index.html?innerAnchor=100026667910&amp;focus=3"
                class="focus-item__core" target="_blank" clstag="h|keycount|head|focus_01" aria-label="推广轮播第1帧"><img
                    class="focus-item-img" src="${item[0].src}"></a>
        </li>            
        `;
    });
    sliderWrapper.innerHTML = lisHtml;

    bannerSwiper = new MySwiper(sliderWrapper, {
        animTimecost: 500,
        interval: 3000,
        sliderWidth: sliderWrapper.children[0].offsetWidth,
        btnPrevSelector: ".slider_control_prev",
        btnNextSelector: ".slider_control_next",
        pointerBoxSelector: ".slider_indicators",
        slidersSelector: "#slider_wrapper>li",
        pointersSelector: ".slider_indicators>i",
        pointerLayout: `<i clstag="h|keycount|head|focus_s01" class="slider_indicators_btn"></i>`,
        pointerActiveClassName: "slider_indicators_btn_active",
    });
}

const fs = document.querySelector(".fs");
const fixedSearch = document.getElementById("fixedSearch");
const elevator = document.querySelector(".elevator_fix");
const btnTop = document.querySelector(".elevator_totop");
const swiperContainer = document.querySelector(".swiper-container");

window.onscroll = function (e) {
    if (window.scrollY >= fs.offsetTop + fs.offsetHeight) {
        fixedSearch.style.top = "0px";

        elevator.style.transition = `all 0.5s linear`;
        elevator.style.position = "fixed";
        elevator.style.top = "75px";
    } else {
        fixedSearch.style.top = "-55px";

        elevator.style.transition = `none`;
        elevator.style.position = "absolute";
        elevator.style.top = "0px";
    }
};

let topTimer = null;
btnTop.onclick = function (e) {
    if (topTimer) return;
    topTimer = setInterval(() => {
        if (document.documentElement.scrollTop > 0) {
            document.documentElement.scrollTop -= 200;
        } else {
            clearInterval(topTimer);
            topTimer = null;
        }
    }, 40);
};

/* swiper */
var swiper = new Swiper(".swiper-container", {
    loop: true,
    speed: 1000,
    slidesPerView: 6,
    spaceBetween: 30,
    // centeredSlides: true,
    watchSlidesProgress: true,
    freeMode: {
        enabled: true,
        momentum: true,
        // sticky: false,
    },
    autoplay: {
        delay: 0,
        waitForTransition: false,
        // disableOnInteraction: true,
    },
});
swiperContainer.onmouseenter = function(e){
    swiper.autoplay.stop()
}
swiperContainer.onmouseleave = function(e){
    swiper.autoplay.start()
}


ajaxPromise({
    url: `${baseUrl}:9000/api/focus`,
}).then((res) => {
    console.log("focus", res);
    renderBanner(res);
});

ajaxPromise({
    url: `${baseUrl}:9000/api/market`,
}).then((res) => {
    // console.log("marketJson", res);
    renderChannels(res);
});

ajaxPromise({
    // url: "http://localhost:9000/api/more2goods",
    url: `${baseUrl}:9000/api/more2goods`,
}).then((res) => {
    console.log("more2goods", res);
    renderMore2goods(res);
});
