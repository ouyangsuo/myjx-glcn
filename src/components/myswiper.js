import { animate } from "../utils/anim_util.js";

export default class MySwiper {
    /* 
    bannerSwiper = new MySwiper(sliderWrapper, {
        animTimecost: 500,
        btnPrevSelector: ".slider_control_prev",
        btnNextSelector: ".slider_control_next",
        pointerBoxSelector:".slider_indicators",
        pointerLayout:`<i clstag="h|keycount|head|focus_s01" class="slider_indicators_btn slider_indicators_btn_active"></i>`
    }
    */
    constructor(domRoot, config) {
        this.initParameters(domRoot, config);

        /* 初始化指示器 */
        this.initPointers();

        this.initSeamlessScroll();

        this.initListeners();

        /* 自动轮播 */
        this.startAutoPlay();
    }

    /* 初识化参数 */
    initParameters(domRoot, config) {
        let {
            animTimecost,
            interval,
            sliderWidth,
            btnPrevSelector,
            btnNextSelector,
            slidersSelector,
            pointersSelector,
            pointerBoxSelector,
            pointerActiveClassName,
            pointerLayout,
        } = config;
        this.animTimecost = animTimecost || 500;
        this.interval = interval || 2000;

        this.banner = domRoot;
        this.ul = domRoot;
        this.btnPrev = document.querySelector(btnPrevSelector);
        this.btnNext = document.querySelector(btnNextSelector);
        this.sliders = domRoot.querySelectorAll(slidersSelector);
        this.pointerBox = document.querySelector(pointerBoxSelector);

        this.sliderWidth = sliderWidth;
        this.pointerLayout = pointerLayout;
        this.pointersSelector = pointersSelector;
        this.pointerActiveClassName = pointerActiveClassName;

        /* 定义变量 */
        this.pointers = null;

        // 当前幻灯片序号
        this.currentIndex = 0;
        // 窗口宽度
        this.bannerWidth = parseFloat(
            window.getComputedStyle(this.banner).width
        );
        // console.log(bannerWidth);
        // 定时器
        this.timer = null;

        // 记录动画是否正在运行
        this.animIsRunning = false;
    }

    /* 移动到目标位置 */
    move() {
        // 标记动画开始
        this.animIsRunning = true;

        animate(
            this.ul,
            {
                left: -this.sliderWidth * (this.currentIndex + 1) + "px",
            },
            this.animTimecost,
            () => {
                // 移形换影【5=0】【-1=4】
                if (this.currentIndex === this.pointers.length) {
                    this.currentIndex = 0;
                    this.ul.style.left =
                        -this.sliderWidth * (this.currentIndex + 1) + "px";
                }
                if (this.currentIndex === -1) {
                    this.currentIndex = this.pointers.length - 1;
                    this.ul.style.left =
                        -this.sliderWidth * (this.currentIndex + 1) + "px";
                }

                // 同步指示器
                this.pointers.forEach((p, index) => {
                    if (index === this.currentIndex) {
                        p.classList.add(this.pointerActiveClassName);
                    } else {
                        p.classList.remove(this.pointerActiveClassName);
                    }
                });

                // 标记动画已停止
                this.animIsRunning = false;
            }
        );
    }

    /* 上一张 */
    prev() {
        // 如果动画正在运行则什么都不做
        if (this.animIsRunning) {
            return;
        }

        // if (currentIndex <= 0) {
        //     currentIndex = lis.length
        // }

        this.currentIndex--;

        // ul向左偏移600px
        this.move();
    }

    /* 下一张 */
    next() {
        // 如果动画正在运行则什么都不做
        if (this.animIsRunning) {
            return;
        }

        // if (currentIndex >= lis.length - 1) {
        //     currentIndex = -1
        // }

        this.currentIndex++;

        // ul向左偏移600px
        this.move();
    }

    /* 初始化指示器 */
    initPointers() {
        // let fragment = document.createDocumentFragment();

        let pointersHtml = "";
        for (let i = 0; i < this.sliders.length; i++) {
            // let li = document.createElement("li");
            // fragment.appendChild(li);
            pointersHtml += this.pointerLayout;
        }
        // this.pointerBox.appendChild(fragment);
        this.pointerBox.innerHTML = pointersHtml;

        this.pointers = this.pointerBox.querySelectorAll(this.pointersSelector);
        this.pointers[0].classList.add(this.pointerActiveClassName);
    }

    /* 开始轮播 */
    startAutoPlay() {
        if (!this.timer) {
            this.timer = window.setInterval(() => this.next(), this.interval);
        }
    }

    /* 停止轮播 */
    stopAutoPlay() {
        if (this.timer) {
            window.clearInterval(this.timer);
            this.timer = null;
        }
    }

    /* 初始化无缝滚动 */
    initSeamlessScroll() {
        let tail = this.sliders[0].cloneNode(true);
        let head = this.sliders[this.sliders.length - 1].cloneNode(true);
        this.ul.appendChild(tail);
        this.ul.insertBefore(head, this.sliders[0]);
    }

    /* 监听器 */
    initListeners() {
        /* 不允许鼠标选中文字 */
        document.onselectstart = (e) => {
            e.preventDefault();
        };

        this.btnPrev.onclick = () => {
            this.prev();
        };
        this.btnNext.onclick = () => {
            this.next();
        };

        /* 鼠标进入banner 停止轮播 */
        this.banner.onmouseenter = (e) => {
            this.stopAutoPlay();
        };

        /* 鼠标离开banner 开始轮播 */
        this.banner.onmouseleave = (e) => {
            this.startAutoPlay();
        };
    }
    
}
