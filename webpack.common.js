const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const { srcPath, distPath } = require("./paths");
// const webpLoader = require("./pack_utils/webp_loader")

module.exports = {
    entry: {
        index: path.join(srcPath, "views/js", "index.js"),
        // other: path.join(srcPath, 'other.js')
    },

    output: {
        // filename: 'bundle.[contentHash:8].js',  // 打包代码时，加上 hash 戳
        filename: "[name].[contentHash:8].js", // name 即多入口时 entry 的 key
        path: distPath,
        // publicPath: 'http://cdn.abc.com'  // 修改所有静态文件 url 的前缀（如 cdn 域名），这里暂时用不到
    },

    module: {
        rules: [
            {
                test: /\.js$/,
                loader: ["babel-loader"],
                include: srcPath,
                exclude: /node_modules/,
            },
            {
                test: /\.html$/,
                use: {
                    loader: "html-withimg-loader",
                    // options:{
                    //     attributes:true
                    //     // attrs:["img:src"]
                    // }
                },
            },
        ],
    },

    plugins: [
        // new HtmlWebpackPlugin({
        //     template: path.join(srcPath, 'index.html'),
        //     filename: 'index.html'
        // })

        // 多入口 - 生成 index.html
        new HtmlWebpackPlugin({
            template: path.join(srcPath, "views/pages", "index.html"),
            filename: "index.html",
            // chunks 表示该页面要引用哪些 chunk （即上面的 index 和 other），默认全部引用
            chunks: ["index", "vendor", "common"], // 要考虑代码分割
        }),
        // 多入口 - 生成 other.html
        // new HtmlWebpackPlugin({
        //     template: path.join(srcPath, 'other.html'),
        //     filename: 'other.html',
        //     chunks: ['other', 'common']  // 考虑代码分割
        // })
    ],
};
